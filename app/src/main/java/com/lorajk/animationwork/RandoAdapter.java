package com.lorajk.animationwork;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by lorajones on 6/25/17.
 */

public class RandoAdapter extends RecyclerView.Adapter<RandoAdapter.ViewHolder> {

    private Context mContext;
    private List<RandoItem> randoItems;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView subtitle;

        public ViewHolder(View itemView) {

            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            subtitle = (TextView) itemView.findViewById(R.id.subtitle);

        }
    }

    public RandoAdapter(Context context, List<RandoItem> items) {
        randoItems = items;
        mContext = context;
    }


    private Context getContext() {
        return mContext;
    }

    @Override
    public RandoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.row_layout, parent, false);

        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RandoAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        RandoItem randoItem = randoItems.get(position);

    }

    @Override
    public int getItemCount() {
        return randoItems.size();
    }

}
