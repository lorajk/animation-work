package com.lorajk.animationwork;

import java.util.ArrayList;

/**
 * Created by lorajones on 6/25/17.
 */

public class RandoItem {

    public String title;
    private String subtitle;

    public RandoItem(String randoTitle, String randoSubtitle) {
        title = randoTitle;
        subtitle = randoSubtitle;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    private static int lastRandoItem = 0;

    public static ArrayList<RandoItem> createItemsList(int numRandoItems) {
        ArrayList<RandoItem> items = new ArrayList<RandoItem>();

        for (int i = 1; i <= numRandoItems; i++) {
            items.add(new RandoItem("RandoTitle", "RandoSubtitle"));
        }

        return items;
    }

}
