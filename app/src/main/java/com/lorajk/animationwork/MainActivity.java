package com.lorajk.animationwork;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<RandoItem> randoItems;
    private SwipeRefreshLayout swipeRefreshLayout;
    Handler myhandler;
    Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rvItems = (RecyclerView) findViewById(R.id.rvItems);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);

        randoItems = RandoItem.createItemsList(10);
        RandoAdapter adapter = new RandoAdapter(this, randoItems);
        rvItems.setAdapter(adapter);
        rvItems.setLayoutManager(new LinearLayoutManager(this));

        r = new Runnable() {
            @Override
            public void run(){
                swipeRefreshLayout.setRefreshing(false);
            }
        };

        final Handler myHandler = new Handler();

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {

                        swipeRefreshLayout.setRefreshing(false);

                        myHandler.postDelayed(r, 1000);
                    }
                }
        );

    }
}
